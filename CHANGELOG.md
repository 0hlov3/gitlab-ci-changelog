## 0.0.2 (2024-11-07)

### added (1 change)

- [Adds the Medium link to the README.md](https://gitlab.com/0hlov3/gitlab-ci-changelog/-/commit/605d181baa5f9bfc5fd8fe010b0ec891c71a6c29)

## 0.0.1 (2024-11-07)

### removed (1 change)

- [Removed: adds example for removed](https://gitlab.com/0hlov3/gitlab-ci-changelog/-/commit/e71e096345b9cee9526fed56dafc0b9f58c4694b)

### security (1 change)

- [Adds an security example to README.md](https://gitlab.com/0hlov3/gitlab-ci-changelog/-/commit/306a5f2ae37e81d7ad590bca7ad3003b0d3a4d3f)

### changed (1 change)

- [Changed the README and Adds some information](https://gitlab.com/0hlov3/gitlab-ci-changelog/-/commit/68ad0abb9af1a0361897763974b2d5bd1e640b63)

### added (1 change)

- [Adds the .gitlab-ci.yml](https://gitlab.com/0hlov3/gitlab-ci-changelog/-/commit/c68cf4aff9157b8776b85c793d3758cf297ca664)
