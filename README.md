# GitLab CI Automated Changelog and Release Generation

This repository serves as a showcase for automating changelog and release page creation using GitLab CI. It demonstrates how to set up a CI/CD pipeline that generates a changelog and release notes automatically for each versioned release.

This project is directly related to the article: [https://blog.schoenwald.aero/awesome-changelog-and-release-page-creation-with-gitlab-ci-409640b5ddd7](https://blog.schoenwald.aero/awesome-changelog-and-release-page-creation-with-gitlab-ci-409640b5ddd7)

## Overview

In this example, you’ll find a [.gitlab-ci.yml](.gitlab/.gitlab-ci.yml) configuration that:

- Uses the GitLab API to create a changelog from commit metadata.
- Produces a CHANGELOG.md file in the repository to document each release's features, fixes, and other updates.

## How It Works

The pipeline includes two primary jobs:

1. create-changelog: Compiles a changelog from the latest commits.
2. create-release-page: Publishes the changelog to the GitLab release page.

For more information on how the pipeline works, check out the full article linked above.

## Example Changes
1. security = This is an Security Fix